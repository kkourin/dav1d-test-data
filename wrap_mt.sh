#!/bin/sh

# if the tests uses the dav1d CLI tool append tile / frame thread options
if [ -z ${1%%*/dav1d} ]; then
    $@ --tilethreads ${TILETHREADS:=2} --framethreads ${FRAMETHREADS:=2}
else
    $@
fi
